#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include <sys/sysinfo.h>
#include "define.hpp"
#include "receiver.hpp"

using namespace std;

#define MB (1024*1024)
#define RND_BLOCK (rand() % (BLK_SIZE)) + ((BLK_SIZE)/2)

void generate_text(char *data, size_t *_size)
{
	uint32_t size = RND_BLOCK;
	if (size > *_size)
	{
		size = *_size;
	}

	for (uint32_t i = 0; i < (size-4); ++i)
	{
		data[i] = 0xBB;
	}
	data[size-1] = '\n';
	data[size-2] = '\r';
	data[size-3] = '\n';
	data[size-4] = '\r';
	*_size = size;
}

void generate_binary(char *data, size_t *_size)
{
	data[0] = BIN_ID;
	uint32_t full_size = RND_BLOCK;
	if (full_size > *_size)
	{
		full_size = *_size;
	}

	uint32_t data_size = full_size - BIN_HEADER;
	*((uint32_t *) (data + 1)) = data_size;
	for (uint32_t i = 0; i < data_size; ++i)
	{
		data[i+BIN_HEADER] = 0x05;
	}
	*_size = full_size;
}

int main(int argc, char **argv)
{
	struct sysinfo info;
	errno = 0;
	if (sysinfo(&info))
	{
		cout << strerror(errno) << endl;
	}

	errno = 0;
	//char *ram = (char *) malloc(info.freeram);
	info.freeram = 1 * 1024;
	char *ram = (char *) malloc(info.freeram);
	if (!ram)
	{
		cout << "Not available memory" << endl;
	}

	size_t i = 0;
	while (i < info.freeram)
	{
		size_t used = info.freeram - i;
		char *avail = ram + i;
		generate_binary(avail, &used);
		i += used;
	}

	Callback object;
	Receiver rec(&object);
	i = 0;
	while (i < info.freeram)
	{
		rec.Receive(ram+i, BLK_SIZE);
		i += BLK_SIZE;
	}	
}