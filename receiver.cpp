#include <cstdint>
#include <cstddef>
#include "define.hpp"
#include "receiver.hpp"

void Receiver::Receive(const char *data, unsigned int _size)
{
	static unsigned int begin = 0;
	char *start;
	unsigned int size;
	if (_size >= BIN_HEADER && data[begin] == BIN_ID)
	{
		// parse binary format
		size = *((uint32_t *) &data[begin+1]);
		start = &data[begin+BIN_HEADER];
		begin += (size + BIN_HEADER);
		obj->BinaryPacket(start, size);
	} else if (_size >= 4)
	{
		// parse string format
		start = (char *) data;
		unsigned int end = begin;
		while (data[end] != '\r'   || 
			   data[end+1] != '\n' ||
			   data[end+2] != '\r' ||
			   data[end+3] != '\n')
		{
			start += OFFSET;
		}
		size = start - data;
		begin += (size + OFFSET);
		obj->TextPacket(start, size);
	}

	rest.first = &data[begin];
	rest.second = _size - size;

	



	// Get invalid data, indicate error
}

void Receiver::reallocate_memory(unsigned int curr_size)
{
	void *tmp;
	if (curr_size > buff_size)
	{
		tmp = new (curr_size);
		if (tmp)
		{
			memmove(tmp, buffer, buff_size);
			delete buff[];
			buff = tmp;
		}
	}
}

Receiver::search_record()
{
	
}

void Callback::BinaryPacket(const char *data, unsigned int size) {}
void Callback::TextPacket(const char *data, unsigned int size) {}