#ifndef _DEFINE_HPP_
#define _DEFINE_HPP_

#define BIN_ID 0x24
#define TEXT_ID
#define BLK_SIZE 1024
#define OFFSET 4
#define TXT_MIN 4 /* 4 bytes for { '\r', '\n', '\r', '\n ' } */
#define BIN_HEADER 5
#define BIN_MIN 5 /* required header size */

#endif