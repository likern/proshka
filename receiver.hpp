#ifndef _RECEIVER_HPP
#define _RECEIVER_HPP

struct IReceiver
{
	virtual void Receive(const char *data, unsigned int size) = 0;
};

struct ICallback
{
	virtual void BinaryPacket(const char *data, unsigned int size) = 0;
	virtual void TextPacket(const char *data, unsigned int size) = 0;
};

class Callback : public ICallback
{
	void BinaryPacket(const char *data, unsigned int size);
	void TextPacket(const char *data, unsigned int size);
};

class Receiver : public IReceiver
{
public:
	Receiver(ICallback *_obj) : obj(_obj) {};
	void Receive(const char *data, unsigned int size);
private:
	reallocate_memory();
	search_record();
	ICallback *obj;
	std::array[1024] tmp;
	unsigned int max_size;
	unsigned int buff_size;
	char buff[2048];
	char *buff;
	std::pair<char *, unsigned int> rest;
};
#endif